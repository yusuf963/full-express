const request = require('supertest')
const app = require('../app')

describe('App', function () {
  it('has the default page', function (done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done)
  })
})

describe('response with 200 status code', () => {
  it('response with 200 status code', (done) => {
    request(app).get('/').expect(200, done)
  })
})

describe('user can sign up', () => {
  it('should receive an object and stor it', () => {
    request(app).post('/signup').expect(201)
  })
})
